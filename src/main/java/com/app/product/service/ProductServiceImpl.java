package com.app.product.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.product.model.Product;
import com.app.product.repository.IndexingRepository;
import com.app.product.repository.ProductRepository;
import com.app.product.util.Util;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private IndexingRepository indexingRepository;

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }
    
    @Override
	public Map<String, Object> paging(Map<String, String> params) {
    	int pageNum = Util.getNum(params.get("pageNum"));
        int pageSize = Util.getNum(params.get("pageSize"));
        
        Integer orderColumn = Util.getInteger(params.get("orderColumn"));
		String orderType = params.get("orderType");
		
		String keyword = params.get("keyword");

		List<Object> items = indexingRepository.getListPaging(keyword, pageNum, pageSize, orderColumn, orderType);
		Long size = indexingRepository.getCountListPaging(keyword);
		
		Map<String, Object> results = new LinkedHashMap<>();
		results.put("list", items);
		results.put("size", size);
		
        return results;
	}

    @Override
    public Product getProductById(Integer id) {
        return productRepository.findById(id).orElse(null);
    }

    @Override
    public List<Product> getProductsByCategory(String category) {
        return productRepository.findByCategory(category);
    }
    
    // Implementasi metode dengan Java Stream
    @Override
    public List<Product> getProductsAboveMinPrice(double minPrice) {
        return productRepository.findAll().stream()
                .filter(product -> product.getPrice() > minPrice)
                .collect(Collectors.toList());
    }

    @Override
    public void save(Product product) throws Exception {
    	if (product.getId() != null) {
    		Product oldProduct = productRepository.findById(product.getId()).orElse(null);
        	
        	if (oldProduct == null) {
        		throw new Exception("Product not found");
        	}
        	
            productRepository.save(product);
    	}
    	else {
    		productRepository.save(product);
    	}
    }
}
