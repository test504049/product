package com.app.product.service;

import java.util.List;
import java.util.Map;

import com.app.product.model.Product;

public interface ProductService {
	
	public List<Product> getAllProducts();
    
	public Map<String, Object> paging(Map<String, String> params);

	public  Product getProductById(Integer id);
    
	public List<Product> getProductsByCategory(String category);
    
    // Menambahkan metode dengan Java Stream
    public List<Product> getProductsAboveMinPrice(double minPrice);
    
    void save(Product product) throws Exception;
    
}
