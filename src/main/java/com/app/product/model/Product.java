package com.app.product.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Index;

@Entity
@Table(name = "product", indexes = {
		@Index(name = "product_idx", columnList="id"),
		@Index(name = "product_namex", columnList="name"),
		@Index(name = "product_categoryx", columnList="category"),
		@Index(name = "product_pricex", columnList="price")
	})
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false)
	private Integer id;

    @Column(name = "name", length = 64, nullable = false)
    private String name;

    @Column(name = "category", length = 64, nullable = false)
    private String category;

    @Column(name = "price", nullable = false)
    private double price;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}