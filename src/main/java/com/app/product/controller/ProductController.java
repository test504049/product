package com.app.product.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.product.model.Product;
import com.app.product.service.ProductService;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public List<Product> getAllProducts() {
        return productService.getAllProducts();
    }
    
    @GetMapping("/paging")
    public Map<String, Object> paging(
            @RequestParam Map<String, String> params) throws Exception {
    	Map<String, Object> paging = productService.paging(params);
    	
    	return paging;
    }

    @GetMapping("/{id}")
    public Product getProductById(@PathVariable Integer id) {
        return productService.getProductById(id);
    }

    @GetMapping("/category/{category}")
    public List<Product> getProductsByCategory(@PathVariable String category) {
        return productService.getProductsByCategory(category);
    }
    
    @GetMapping("/above-min-price")
    public List<Product> getProductsAboveMinPrice(@RequestParam double minPrice) {
    	return productService.getProductsAboveMinPrice(minPrice);
    }

    @PostMapping
    public void save(@RequestBody Product product) throws Exception {
        productService.save(product);
    }
}
