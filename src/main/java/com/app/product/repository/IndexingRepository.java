package com.app.product.repository;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

@Repository
public class IndexingRepository {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<Object> getListPaging(String keyword, int pageNum, int pageSize, Integer orderColumn, String orderType) {
		Double keywordPrice = null;
		
		if (StringUtils.isNotEmpty(keyword)){
			try {
				keywordPrice = Double.parseDouble(keyword);
			} catch (Exception e) {
				
			}
		}
		
		String[] dataColumnOrder = {"t.id", "t.name", "t.category", "t.price"};
		
		String query =" SELECT "
			    	+ " 	t.id as id, "
				    + "		t.name as name,"
				    + "		t.category as category, "
				    + "		t.price "
					+ " FROM "
					+ " 	product t ";
		
		if (StringUtils.isNotEmpty(keyword)){
			query += " WHERE ( "
	  				+ "		t.name LIKE :keyword "
	  				+ "		OR t.category LIKE :keyword ";
				
			if (keywordPrice != null){
				query +=  "	OR t.price = :keywordPrice ";
			}
				
			query += " ) ";
		}
		
		if (orderColumn != null && StringUtils.isNotEmpty(orderType)) {
			query += " ORDER BY " + dataColumnOrder[orderColumn] + " " + orderType;
		}
		else {
			query += " ORDER BY t.id DESC ";
		}
		
		query += " LIMIT :limit "
			   + " OFFSET :offset ";
		
		Query q = entityManager.createNativeQuery(query);
		
		if (StringUtils.isNotEmpty(keyword)){
			q.setParameter("keyword", "%"+keyword+"%");
				
			if (keywordPrice != null){
				q.setParameter("keywordPrice", keywordPrice);
			}
		}

		q.setParameter("limit", pageSize);
		q.setParameter("offset", (pageNum - 1) * pageSize);
		
		List<Object[]> results =  q.getResultList();
		
		List<Object> products = new ArrayList<>();
		
		for (Object[] row : results) {
			Map<String, Object> product = new LinkedHashMap<>();
			product.put("id", (Integer) row[0]);
			product.put("name", (String) row[1]);
			product.put("category", (String) row[2]);
			product.put("price", (Double) row[3]);
			
			products.add(product);
		}
		
		return products;
	}
	
	public Long getCountListPaging(String keyword) {
		Double keywordPrice = null;
		
		if (StringUtils.isNotEmpty(keyword)){
			try {
				keywordPrice = Double.parseDouble(keyword);
			} catch (Exception e) {
				
			}
		}
		
		String query =" SELECT "
					+ " 	count(t.id) "
					+ " FROM "
					+ " 	product t ";
		
		if (StringUtils.isNotEmpty(keyword)){
			query += " WHERE ( "
	  				+ "		t.name LIKE :keyword "
	  				+ "		OR t.category LIKE :keyword ";
				
			if (keywordPrice != null){
				query +=  "	OR t.price = :keywordPrice ";
			}
				
			query += " ) ";
		}
		
		Query q = entityManager.createNativeQuery(query);
		
		if (StringUtils.isNotEmpty(keyword)){
			q.setParameter("keyword", "%"+keyword+"%");
				
			if (keywordPrice != null){
				q.setParameter("keywordPrice", keywordPrice);
			}
		}
		
		return ((Number) q.getSingleResult()).longValue();
	}

}
