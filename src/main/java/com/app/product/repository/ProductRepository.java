package com.app.product.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.product.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {
	
    List<Product> findByCategory(String category);
    
    // Menambahkan metode dengan Native SQL Query
    @Query(value = "SELECT * FROM product WHERE price > :minPrice", nativeQuery = true)
    List<Product> findProductsAboveMinPrice(double minPrice);
    
}
