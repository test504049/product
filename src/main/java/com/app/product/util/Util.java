package com.app.product.util;

import java.util.logging.Logger;

public class Util {
	
	public static int getNum(Object data) {
		int result = 0;
		
		if ((data != null) && !"".equals(data)) {
			try {
				result = Integer.parseInt(String.valueOf(data));
			} catch (Exception e) {
				Logger.getLogger("Failed");
			}
		}
		
		return result;
	}
	
	public static Integer getInteger(Object data) {
		Integer result = null;
		
		if ((data != null) && !"".equals(data)) {
			try {
				result = Integer.parseInt(String.valueOf(data));
			} catch (Exception e) {
				Logger.getLogger("Failed");
			}
		}
		
		return result;
	}

}
