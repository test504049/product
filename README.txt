Noted:
Make sure Docker is installed.


Step by step
1. open cmd

2. Run the following script:
cd /path/to/your/project

mvn clean install -DskipTests

move /Y target\product-service-1.0.0.jar
move /Y target\product-service-1.0.0.jar.original

docker-compose -f docker-compose.yml rm -f -s

docker image prune -a

docker-compose -f docker-compose.yml up -d